# Java Selenium Framework

### Multiple environments
Similar as for multiple domains the tests can also be run on multiple environments,
for example a staging and a live environment. It is as simple as changing a spring profile parameter.

### Headless
The option to run the tests in headless mode (without opening a browser).

### Allure reports integration
[Allure Framework](https://github.com/allure-framework/allure2) is a flexible lightweight multi-language test report tool that shows a very concise representation of what has been tested in a neat web report form

 Prerequisites

Dependencies versions can be found in project Maven POM file.

* Java 11 or higher
* Maven
* Spring Boot
* TestNG
* Allure


## How-to
### Local on different browsers
To run the tests on your local machine use the command:


The parameters explained:
* `spring.profiles.active` - defines on which environment the tests will run
* `browser` - defines the browser (`chrome/firefox`)


### Headless
The latest versions of the Chrome and Firefox browsers have the feature to run browsers in headless mode. This speeds up the running time of the tests immensely. For running the tests using the headlless mode feature:

where as browser either `chrome` or `firefox` can be selected.

## Reports

Reporting of the tests results is one of the most important aspects of a framework. It doesn't matter how many and good your tests are when they are not enough visible. In this framework Allure was used to generate
visually rich and insightful reports.

![Allure Report](.github/allure-img.png)

Allure has to be installed before being able to show test results:

```
$ brew untap qameta/allure
$ brew update
$ brew install allure
```

After every test run the results are automatically stored in an `allure-results` directory. The results then can be seen locally by running:

```
allure serve
```

or in all popular CI's with the help of allure plugins.
