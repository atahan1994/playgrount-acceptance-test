package selenium.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import selenium.context.Base;
import selenium.driver.Driver;
import selenium.pageobject.YourPlayground;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class YourPlaygroundTests extends Base {

  private final int defaultGridSize = 16;

  @Story("As a user I want to click all icons in the outer perimeter in order to see the dialog should appear")
  @Description("Verify default grid size, select perimeter and validate if selected")
  @Test(groups = {"playground", "smoke"})
  @Issue("PLAYG-1")
  // Wrote Failed test - last selected element before dialog appears doesn't become activate
  public void selectGridPerimeterAndVerify() {
    YourPlayground yourPlayground = new YourPlayground(Driver.getDriver());
    Integer currentGridSize = yourPlayground.calculateGridSize();
    assertThat(currentGridSize, equalTo(defaultGridSize));
    yourPlayground.selectOuterPerimeterAndVerifyIsSelected();
  }

  @Story("As a user I want to be able to define size of the grid")
  @Description("Trigger dialog and verify: text and and invalid Integer value passed")
  @Test(groups = {"playground", "smoke"})
  @Issue("PLAYG-4")
  // Wrote Success test
  public void verifyDialogWithInvalidValueInteger() {
    YourPlayground yourPlayground = new YourPlayground(Driver.getDriver());
    yourPlayground.triggerDialog();
    yourPlayground.dialogSendInvalidValueAndVerify("12");
    Integer currentGridSize = yourPlayground.calculateGridSize();
    assertThat(currentGridSize, equalTo(defaultGridSize));
  }

  @Story("Zenitechh")
  @Description("Trigger dialog and verify: text and and invalid string value passed")
  @Test(groups = {"playground", "smoke"})
  // Wrote Failed test - dialog doesn't validate invalid string value in any way , it just disappears
  public void verifyDialogWithInvalidValueString() {
    YourPlayground yourPlayground = new YourPlayground(Driver.getDriver());
    yourPlayground.triggerDialog();
    yourPlayground.dialogSendInvalidValueAndVerify("testString");
    Integer currentGridSize = yourPlayground.calculateGridSize();
    assertThat(currentGridSize, equalTo(defaultGridSize));
  }

  @Test(groups = {"playground", "smoke"})
  @Story("As a user I want to be able to define size of the grid")
  @Description("Trigger dialog : change grid size to 9 and validate,After the user defines the size of the grid, the grid should have appropriate number of columns and rows.")
  @Issue("PLAYG-4")
  // Wrote Success
  public void changeGridSize() {
    YourPlayground yourPlayground = new YourPlayground(Driver.getDriver());
    yourPlayground.triggerDialog();
    int gridResizeValue = 9;
    yourPlayground.dialogSendValidValue(String.valueOf(gridResizeValue));
    Integer currentGridSize = yourPlayground.calculateGridSize();
    assertThat(currentGridSize, equalTo(gridResizeValue * gridResizeValue));
  }

  @Test(groups = {"playground", "smoke"})
  @Story("As a user, i want to see appropriate number of columns and rows on the grid")
  @Description("Trigger dialog : change grid size to 3 and validate,After the user defines the size of the grid, the grid should have appropriate number of columns and rows.")
  @Issue("PLAYG-5")
  // Wrote Success test
  public void changeGridSizeTo3() {
    YourPlayground yourPlayground = new YourPlayground(Driver.getDriver());
    yourPlayground.triggerDialog();
    int gridResizeValue = 3;
    yourPlayground.dialogSendValidValue(String.valueOf(gridResizeValue));
    Integer currentGridSize = yourPlayground.calculateGridSize();
    assertThat(currentGridSize, equalTo(gridResizeValue * gridResizeValue));
    yourPlayground.triggerDialog();
    yourPlayground.dialogSendInvalidValueAndVerify("2");
  }

}
