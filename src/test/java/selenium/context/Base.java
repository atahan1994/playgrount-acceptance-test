package selenium.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Listeners;
import selenium.Application;
import selenium.listeners.WebDriverListener;

@Listeners(WebDriverListener.class)
@SpringApplicationConfiguration(Application.class)
public abstract class Base extends AbstractTestNGSpringContextTests {

    @Autowired
    Environment environment;

    public String getAppUrl() {
        return environment.getProperty("appUrl");
    }

    public String getBrowser() {
        return environment.getProperty("browser");
    }
}
