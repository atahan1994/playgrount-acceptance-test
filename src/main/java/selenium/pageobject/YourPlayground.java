package selenium.pageobject;

import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.driver.Driver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class YourPlayground extends Driver {

    private final String dialogContent = "Done! Ready for the next try? Give me a size [3-9]:";
    private final String dialogInvalidValue = "Not a good size!";

    private WebElement[][] gridMatrix;
    private Alert alert;

    @FindBy(css = ".mainGrid")
    private WebElement mainGrid;

    public YourPlayground(WebDriver driver) {
        super(driver);
    }

    private List<WebElement> getGridRows() {
        waitForElement(mainGrid);
        return mainGrid.findElements(By.cssSelector(".row"));
    }

    private List<WebElement> getRowColumns(WebElement row) {
        return row.findElements(By.cssSelector(".icon"));
    }

    public Integer calculateGridSize() {
        constructGridMatrix();
        return gridMatrix.length * gridMatrix[0].length; // first dimension * second dimension
    }

    public WebElement[][] constructGridMatrix() {
        List<WebElement> rows = getGridRows();
        gridMatrix = new WebElement[rows.size()][rows.size()];
        for (int i = 0; i < rows.size(); i++) {
            List<WebElement> columns = getRowColumns(rows.get(i));
            for (int j = 0; j < columns.size(); j++) {
                gridMatrix[i][j] = columns.get(j);
            }
        }
        return gridMatrix;
    }

    public void selectOuterPerimeterAndVerifyIsSelected() {
        constructGridMatrix();
        for (int row = 0; row < gridMatrix.length; row++) {
            for (int col = 0; col < gridMatrix[0].length; col++) {
                if (row == 0 || row == gridMatrix.length - 1) {
                    gridMatrix[row][col].click();
                    assertThat(gridMatrix[row][col].getAttribute("active"), equalTo("true"));
                } else if (col == 0 || col == gridMatrix[0].length - 1) {
                    gridMatrix[row][col].click();
                    assertThat(gridMatrix[row][col].getAttribute("active"), equalTo("true"));
                } else {
                    assertThat(gridMatrix[row][col].getAttribute("active"), equalTo(null));
                }
            }
        }
    }

    public void triggerDialog() {
        try {
            selectOuterPerimeterAndVerifyIsSelected();
        } catch (UnhandledAlertException exception) {
            alert = switchToAlertWindow();
        }
    }

    public void dialogSendInvalidValueAndVerify(String invalidValue) {
        assertThat(alert.getText(), equalTo(dialogContent));
        alert.sendKeys(invalidValue);
        alert.accept();
        alert = switchToAlertWindow();
        assertThat(alert.getText(), equalTo(dialogInvalidValue));
        alert.accept();
    }

    public void dialogSendValidValue(String validValue) {
        assertThat(alert.getText(), equalTo(dialogContent));
        alert.sendKeys(validValue);
        alert.accept();
    }
}
